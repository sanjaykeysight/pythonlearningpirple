# Homework Assignment #4: Lists

'''
Author : Sanjay Kumar
Last Updated : 24 Jan 2019
Purpose : Create an empty list, add new item to if it doesn't already exists

'''

myUniqueList = []

def AddElementToList(input):
    if input in myUniqueList:
        return False
    else:
        myUniqueList.append(input)
        return True




if AddElementToList("Sanjay"):
    print ("Element added to list")
else:
    print ("Element already exists in list")

if AddElementToList("David"):
    print ("Element added to list")
else:
    print ("Element already exists in list")

if AddElementToList("Sanjay"):
    print ("Element added to list")
else:
    print ("Element already exists in list")

print (myUniqueList)
