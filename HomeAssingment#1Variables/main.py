# Homework Assignment #1: Variables

'''
Author : Sanjay Kumar
Last Updated : 22 Jan 2019
Purpose : Display information about my favorite song
'''

SongTitle = "Wo rang bhi kya rang hai"
movie = "Zero"
musician = "Ajay Atul"
SongWriter = "Irshad Kamil"
cast = "Shahrukh Khan, Katrina Kaif, Anushka Sharma"
duration = 5.18
print ("="*50)
print ("Lyrics : " + SongTitle)
print ("From Movie : " + movie)
print ("Screen Cast : " + cast)
print ("Writer : " + SongWriter)
print ("Duration : " + str(duration) + " minutes")
print ("="*50)
