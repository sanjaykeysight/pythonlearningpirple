# Homework Assignment #2: Functions

'''
Author : Sanjay Kumar
Last Updated : 22 Jan 2019
Purpose : Display information about my favorite song
          Check if a particular movie name is there
'''

SongTitle = "Wo rang bhi kya rang hai"
movie = "Zero"
musician = "Ajay Atul"
SongWriter = "Irshad Kamil"
cast = "Shahrukh Khan, Katrina Kaif, Anushka Sharma"
duration = 5.18

def Duration ():
    return duration

def Movie ():
    return movie

def SongTitle():
    return SongTitle

def CheckMovie(movieToCheck):
    if movieToCheck == movie:
        return True
    else:
        return False


print ("="*50)
print ("Lyrics : " + str(SongTitle()))
print ("From Movie : " + str(Movie()))
print ("Screen Cast : " + str(Duration()))
print ("Check if movie name is 'Zero' : " + str(CheckMovie("Zero")))
print ("Check if movie name is 'ZZeerroo' : " + str(CheckMovie("ZZeerroo")))
print ("="*50)
