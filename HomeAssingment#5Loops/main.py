# Homework Assignment #2: Functions

'''
Author : Sanjay Kumar
Last Updated : 28 Jan 2019
Purpose : Loop assingment
          print fizz if number divisible by 3
          print buzz if number divisible by 5
          print fizzbuzz if number divisible by 3 and 5

'''
def CheckPrime (num):
    if num > 2:
        flag = True
        for i in range(2, num - 1):
            if num % i == 0:
                flag = False
                break
    else:
        flag = False
        return flag # for number 2 which is prime

    return flag

for num in range(1, 100):
    if num % 3 == 0 and num % 5 == 0:
        if CheckPrime(num):
            print (str(num) + " : fizzbuzz + Prime")
        else:
            print (str(num) + " : fizzbuzz")
    elif num % 5 == 0:
        if CheckPrime(num):
            print (str(num) + " : buzz + Prime")
        else:
            print (str(num) + " : buzz")
    elif num % 3 == 0:
        if CheckPrime(num):
            print (str(num) + " : fizz + Prime")
        else:
            print (str(num) + " : fizz")
    elif CheckPrime(num):
        print (str(num) + " : Prime")
