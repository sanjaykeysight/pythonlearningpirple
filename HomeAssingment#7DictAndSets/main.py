# Homework Assignment #1: Variables

'''
Author : Sanjay Kumar
Last Updated : 01 Feb 2019
Purpose : Display information about my favorite song using dictionary
'''

myFavSongDict = { "SongTitle" : "Wo rang bhi kya rang hai",
                  "movie" : "Zero",
                  "musician" : "Ajay Atul",
                  "SongWriter" : "Irshad Kamil",
                  "cast" : "Shahrukh Khan, Katrina Kaif, Anushka Sharma",
                  "duration" : 5.18
                 }

def guessValue(k, v):
    if k in myFavSongDict:
        if myFavSongDict[k] == v: ## If dict key exists then check its value
            return True
        else:
            return False
    else:
        return False

for attr in myFavSongDict:
    print (attr + " : " + str(myFavSongDict[attr]) )

# Guessing Movie name
print ("My Guess Movie name is Zero")
if guessValue("movie", "Zero"):
    print ("\tYou guess it right")
else:
    print ("\tBetter luck next time")
