'''
Author : Sanjay Kumar
Last Updated : 30 Jan 2019
Purpose : Advanced Loop assingment
          Draw a playing of nxm dimension

'''

def DrawBoard(row, col):
    #"1234567"
    #" - - - "
    #"| | | |"
    #" - - - "
    #"| | | |"
    for r in range(1, row * 2):
        for c in range(1, col * 2 + 2):
            if r % 2 != 0 and c % 2 == 0:
                print (" -", end="")
            if r % 2 == 0 and c % 2 != 0:
                print("| ", end="")
        print("")


DrawBoard(10,8)
