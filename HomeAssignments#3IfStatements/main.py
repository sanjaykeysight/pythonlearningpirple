# Homework Assignment #2: Functions

'''
Author : Sanjay Kumar
Last Updated : 22 Jan 2019
Purpose : out of three check if any of two numbers are equal
          number can be given as integer or string, function will handle both

'''

def compare(a, b, c):
    if int(a) == int(b):  # use int so that even string can be compared againt int
        return True
    elif int(a) == int(c):
        return True
    elif int(b) == int(c):
        return True
    else:
        return False

one = 1
two = 2
three = "2"
retVal = compare(one, two, three)
if retVal == False:
    print ("None of the numbers are equal from : " + str(one) + " " + str(two) + " " + str(three))
else:
    print ("Atleast two numbers are equal from : " + str(one) + " " + str(two) + " " + str(three))
